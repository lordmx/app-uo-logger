/**
 * @apiDefine Metadata
 * @apiSuccess (Success 2xx) {Object} metadata Meta-data
 * @apiSuccess (Success 2xx) {Number} metadata.total Total items found
 * @apiSuccess (Success 2xx) {Number} metadata.limit Items per page
 * @apiSuccess (Success 2xx) {Number} metadata.pages Total pages got
 * @apiSuccess (Success 2xx) {Number} metadata.page Current page
 * @apiSuccess (Success 2xx) {String[]} metadata.fields Fields selected
 * @apiSuccess (Success 2xx) {Object} metadata.sort Sorting data
 */