/**
 * @apiDefine HeaderAuthorization
 * @apiHeader (Authorization) {String} Authorization Authorization via token.
 * @apiHeaderExample {json} Example:
 *     {
 *       "Authorization": "Bearer 5ab4a2002b63075e4f65c853"
 *     }
 */

/**
 * @apiDefine HeaderContentType
 * @apiHeader (ContentType) {String} Content-Type Content's type
 * @apiHeaderExample {json} Example:
 *     {
 *       "Content-type": "application/json"
 *     }
 */