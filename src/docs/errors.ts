/**
 * @apiDefine Error404
 * @apiError NotFound
 *
 * @apiErrorExample Example:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "code": 404,
 *       "message": "Not Found"
 *     }
 */

/**
 * @apiDefine Error400
 * @apiError BadRequest
 *
 * @apiErrorExample Example:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "code": 400,
 *       "message": "Bad Request"
 *     }
 */

/**
 * @apiDefine Error401
 * @apiError Unauthorized
 *
 * @apiErrorExample Пример:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "code": 401,
 *       "message": "Unauthorized"
 *     }
 */

/**
 * @apiDefine Error405
 * @apiError MethodNotAllowed
 *
 * @apiErrorExample Пример:
 *     HTTP/1.1 405 Method Not Allowed
 *     {
 *       "code": 405,
 *       "message": "Method Not Allowed"
 *     }
 */

/**
 * @apiDefine Error500
 * @apiError InternalServerError
 *
 * @apiErrorExample Пример:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "code": 500,
 *       "message": "Internal Server Error"
 *     }
 */