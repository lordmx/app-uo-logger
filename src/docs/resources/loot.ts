/**
 * Loot log
 *
 * @apiDefine Loot
 * @apiSuccess (Success 2xx) {Object[]} list Loot log
 * @apiSuccess (Success 2xx) {String} list.id Log record ID
 * @apiSuccess (Success 2xx) {String} list.character_id Character ID
 * @apiSuccess (Success 2xx) {String} list.created_at Date-time of loot's fact
 * @apiSuccess (Success 2xx) {String} list.name Item's title
 * @apiSuccess (Success 2xx) {String[]} list.properties Item's properties
 */

/**
 * @apiDefine LootExample
 * @apiSuccessExample Example:
 *     HTTP/1.1 200 OK
 *     {
 *              "list": [
 *                   {
 *                      "id": "5be20b9069078c639c381468",
 *                      "character_id": "5be20b9069078c639c381468",
 *                      "created_at": "2012-12-20T10:00:00Z"
 *                      "name": "Pitchfork"
 *                      "properties": [
 *                          "Weight: 11 Stones",
 *                          "Durability +100",
 *                          "Splintering Weapon 20%"
 *                      ]
 *                   }
 **              ],
 *              "metadata": {
 *                  "total": 1,
 *                  "limit": 1,
 *                  "pages": 1,
 *                  "page": 1,
 *                  "fields": [],
 *                  "sort": {}
 *              },
 *              "error": null
 *     }
 */