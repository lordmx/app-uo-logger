/**
 * Characters
 *
 * @apiDefine Character
 * @apiSuccess (Success 2xx) {Object[]} list Characters
 * @apiSuccess (Success 2xx) {String} list.id Character ID
 * @apiSuccess (Success 2xx) {String} list.status Last stats (idle, farming, dead)
 * @apiSuccess (Success 2xx) {String} list.name Character's name
 * @apiSuccess (Success 2xx) {String} list.last_activity_date Date-time of character's last activity
 * @apiSuccess (Success 2xx) {String} list.last_screen_url Screen picture
 */

/**
 * @apiDefine CharacterExample
 * @apiSuccessExample Example:
 *     HTTP/1.1 200 OK
 *     {
 *              "list": [
 *                   {
 *                      "id": "5be20b9069078c639c381468",
 *                      "name": "Farmer 1",
 *                      "status": "farming",
 *                      "last_activity_date": "2012-12-20T10:00:00Z",
 *                      "last_screen_url": "http://example.com/sc/farmer_1.jpg"
 *                   }
 *              ],
 *              "metadata": {
 *                  "total": 1,
 *                  "limit": 1,
 *                  "pages": 1,
 *                  "page": 1,
 *                  "fields": [],
 *                  "sort": {}
 *              },
 *              "error": null
 *     }
 */
