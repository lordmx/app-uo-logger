/**
 * @apiDefine ParamSort
 * @apiParam {String} sort
 */

/**
 * @apiDefine ParamFields
 * @apiParam {String} fields
 */

/**
 * @apiDefine ParamLimit
 * @apiParam {Number} limit
 */

/**
 * @apiDefine ParamPage
 * @apiParam {Number} page
 */