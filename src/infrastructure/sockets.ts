import { Socket } from "socket.io";
import { Character } from "../models/character";
import { Service } from "typedi";

@Service()
export class SocketManager {
    private sockets: Map<Character, Socket> = new Map();

    /**
     * @param {Character} character
     * @param {Socket} socket
     */
    register(character: Character, socket: Socket) {
        this.sockets.set(character, socket);
    }

    /**
     * @param {Character} character
     */
    unregister(character: Character) {
        this.sockets.delete(character);
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isRegistered(character: Character): boolean {
        return this.sockets.has(character);
    }

    /**
     * @param {Character} character
     * @param {object} message
     */
    send(character: Character, message: object) {
        const socket: Socket = this.sockets.get(character);

        if (socket && socket.connected) {
            socket.emit('action', message);
        }
    }
}