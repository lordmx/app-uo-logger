import { ServiceBroker, Logger, LogLevels, ServiceSchema } from 'moleculer';
import { Service } from "typedi";
import { Config } from "../config";

export interface IServiceBus {
    /**
     * @param {string} event
     * @param {object} message
     */
    publish(event: string, message: object);

    /**
     * @param {string} event
     * @param {object} message
     */
    broadcast(event: string, message: object);

    /**
     * @param {string} event
     * @param {(message: object) => void} callback
     */
    subscribe(event: string, callback: (message: object) => void);

    /**
     * @param {object} service
     */
    register(service: object);

    start(): void;
}

@Service()
export class ServiceBus implements IServiceBus {
    protected broker: ServiceBroker;
    protected loggers: Map<string, Logger>;

    /**
     * @param {Config} config
     */
    constructor(private config: Config) {
        this.broker = new ServiceBroker({
            namespace: config.bus.namespace,
            nodeID: config.bus.nodeID,
            logger: config.bus.logger.toString() !== config.bus.logger
                ? <boolean>config.bus.logger
                : (config.bus.logger in this.loggers
                    ? this.loggers[<string>config.bus.logger]
                    : false
                ),
            logLevel: <LogLevels>config.bus.logLevel,
            logFormatter: config.bus.logFormatter,
            transporter: config.bus.transporter,
            cacher: config.bus.cacher,
            serializer: config.bus.serializer,
            requestTimeout: config.bus.requestTimeout,
            retryPolicy: {
                enabled: config.bus.requestRetryCount > 0,
                retries: config.bus.requestRetryCount,
                delay: config.bus.requestRetryDelay,
                maxDelay: config.bus.requestMaxRetryDelay,
                check: null,
            },
            maxCallLevel: config.bus.maxCallLevel,
            heartbeatInterval: config.bus.heartbeatInterval,
            heartbeatTimeout: config.bus.heartbeatTimeout,
            disableBalancer: false,
            registry: {
                strategy: config.bus.registryStrategy,
                preferLocal: true
            },
            circuitBreaker: {
                enabled: false,
            },
            validation: true,
            validator: null,
            metrics: false,
            metricsRate: 1,
            internalServices: true,
            hotReload: false,

            // Register middlewares
            middlewares: [],

            // Called after broker created.
            created(broker) {

            },

            // Called after broker starte.
            started(broker) {

            },

            // Called after broker stopped.
            stopped(broker) {

            }
        });

    }

    public start(): void {
        this.broker.start();
    }

    /**
     * @param {string} name
     * @param {Moleculer.Logger} logger
     */
    public registerLogger(name: string, logger: Logger) {
        this.loggers[name] = logger;
    }

    /**
     * @param {string} event
     * @param {object} message
     */
    public publish(event: string, message: object) {
        this.broker.emit(event, message);
    }

    /**
     * @param {string} event
     * @param {object} message
     */
    public broadcast(event: string, message: object) {
        this.broker.broadcast(event, message);
    }

    /**
     * @param {string} event
     * @param {(message: object) => void} callback
     */
    public subscribe(event: string, callback: (message: object) => void) {

    }

    /**
     * @param {object} service
     */
    public register(service: object): void {
        this.broker.createService(<ServiceSchema>service);
    }
}
