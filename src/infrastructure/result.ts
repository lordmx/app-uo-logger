import { QueryFilterOpt } from "./query";

export class ResultSet<T extends { toJSON: Function }> {
    private list: T[];
    private count: number;
    private opt: QueryFilterOpt;

    /**
     * @param {T[]} list
     * @param {number} count
     * @param {QueryFilterOpt} opt
     */
    constructor(list: T[], count: number, opt: QueryFilterOpt) {
        this.list = list;
        this.count = count;
        this.opt = opt;
    }

    toJSON(): object {
        return {
            metadata: {
                total: this.count,
                limit: this.opt.getLimit(),
                page: this.opt.getPage(),
                pages: Math.ceil(this.count / this.opt.getLimit()),
                fields: this.opt.getFields(),
                sort: this.opt.getSort(),
            },
            result: this.list.map(item => {
                return ('toJSON' in item) ? item.toJSON() : item
            }),
            error: null,
        };
    }
}