import { Context } from 'koa';
import { unique } from "./utils";

export const DEFAULT_LIMIT = 20;

export class QueryFilterOpt {
    public limit: number = DEFAULT_LIMIT;
    public page: number = 1;
    public sort: string = '';
    public fields: string = '';

    /**
     * @return {number}
     */
    public getOffset(): number {
        return (this.getPage() - 1) * this.getLimit();
    }

    /**
     * @return {number}
     */
    public getLimit(): number {
        let limit = this.limit || DEFAULT_LIMIT;

        if (this.limit <= 0) {
            limit = DEFAULT_LIMIT;
        }

        return Math.max(limit, 1);
    }

    /**
     * @return {number}
     */
    public getPage(): number {
        return Math.max(this.page || 1, 1);
    }

    /**
     * @return {string[]}
     */
    public getFields(): string[] {
        return unique((this.fields || '')
            .split(',')
            .map((field: string) => field.trim())
            .filter(item => item)
        );
    }

    /**
     * @return {Map<string, number>}
     */
    public getSort(): Map<string, number> {
        let result: Map<string, number> = new Map();

        (this.sort || '')
            .split(',')
            .map(item => item.trim())
            .filter(item => item)
            .forEach((part: string) => {
                let direction;

                switch (part.substr(0, 1)) {
                    case '-':
                        part = part.substr(1);
                        direction = -1;
                        break;

                    case '+':
                        part = part.substr(1);

                    default:
                        direction = 1;
                }

                result.set(part, direction);
            });

        return result;
    }

    /**
     * @param {Context} ctx
     * @return {QueryFilterOpt}
     */
    static fromContext(ctx: Context): QueryFilterOpt {
        let res = new QueryFilterOpt();

        res.sort = ctx.query['sort'] || '';
        res.fields = ctx.query['fields'] || '';
        res.page = ctx.query['page'] || 1;
        res.limit = ctx.query['limit'] || DEFAULT_LIMIT;

        return res;
    }
}