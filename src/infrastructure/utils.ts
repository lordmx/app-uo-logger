export const unique = (arr: Array<any>): Array<any> => {
    return arr.filter((item, index, array) => index == array.indexOf(item));
};

export const mapToObject = (map: Map<any, any>): Object => {
    return Object.assign(
        {},
        ...Array.from(
            map.entries(),
            ([k, v]) => ({[k]: v})
        )
    )
};

export function objectToMap<K, V>(object: Object): Map<K, V> {
    let result = new Map<K, V>();

    Object.keys(object || {}).forEach((key: string) => {
        result.set(<any>key as K, (object || {})[key]);
    });

    return result;
};