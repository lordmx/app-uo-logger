export class Pair<A, B> {
    private a: A;
    private b: B;

    /**
     * @param {A} a
     * @param {B} b
     */
    constructor(a: A, b: B) {
        this.a = a;
        this.b = b;
    }

    /**
     * @return {A}
     */
    getA(): A {
        return this.a;
    }

    /**
     * @return {B}
     */
    getB(): B {
        return this.b;
    }

    /**
     * @param {Array<any>}arr
     * @return {Pair}
     */
    static fromArray(arr: Array<any>) {
        return new Pair(arr[0], arr[1]);
    }
}