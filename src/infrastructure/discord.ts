import {
    Client,
    Message,
    Channel,
    Guild,
    TextChannel
} from 'discord.js';
import { Service } from 'typedi';
import { Config } from "../config";
import { CommandManager } from "../commands/manager";

@Service()
export class Discord {
    private client: Client;

    /**
     * @param {Config} config
     * @param {CommandManager} commandManager
     */
    constructor(private config: Config, private commandManager: CommandManager) {
        this.client = new Client();

        this.client.on('ready', async () => {
            console.info(`Discord ready. Logged in as ${this.client.user.tag}!`);
            this.client.user.setActivity(`Serving ${this.client.guilds.size} servers`);
        });

        this.client.on("guildCreate", (guild: Guild) => {
            console.info(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
            this.client.user.setActivity(`Serving ${this.client.guilds.size} servers`);
        });

        this.client.on("guildDelete", (guild: Guild)  => {
            console.info(`I have been removed from: ${guild.name} (id: ${guild.id})`);
            this.client.user.setActivity(`Serving ${this.client.guilds.size} servers`);
        });

        this.client.on("message", async (message: Message) => {
            if (message.author.bot) {
                return;
            }

            if (message.content.indexOf(config.discord.prefix) !== 0) {
                return;
            }

            const args = message.content
                .slice(config.discord.prefix.length)
                .trim()
                .split(/ +/g)
                .filter(item => item);

            const command = args.shift().toLowerCase();
            this.commandManager.execute(command, message, args);

        });

        this.client.login(config.discord.token);
    }

    /**
     * @param {string} to
     * @param {string} message
     * @return {Promise<Message>}
     */
    async send(to: string, message: string): Promise<Message> {
        const channel: Channel = this.client.channels.get(to);

        console.info(`[Discord] Sending to channel "${to}": `, message);

        if (channel) {
            return <Promise<Message>>(<TextChannel>channel).send(message);
        }

        return Promise.resolve(null);
    }
}