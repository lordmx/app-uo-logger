import { Notification } from "./interface";
import { Character, CharacterProp } from "../models/character";
import { LACK_OF_SHIELDS } from "../constants/notifications";

export const MIN_SHIELDS = 1;

export class LastShield implements Notification {
    /**
     * @param {Character} character
     * @return {string}
     */
    formatMessage(character: Character): string {
        const payload = character.getPayload();
        const shieldsCount: number = Number.parseInt(payload.get(CharacterProp.remainderShields));
        const s = MIN_SHIELDS > 1 ? 's' : '';

        return `Last ${shieldsCount} shield${s} remain${s} in the "${character.getName()}"\`s pack`;
    }

    /**
     * @return {number}
     */
    getInterval(): number {
        return 15 * 60;
    }

    /**
     * @return {string}
     */
    getName(): string {
        return LACK_OF_SHIELDS;
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isSatisfiedBy(character: Character): boolean {
        const payload = character.getPayload();
        const shieldsCount: number = Number.parseInt(payload.get(CharacterProp.remainderShields));

        return !character.isDead() && shieldsCount <= MIN_SHIELDS;
    }

}