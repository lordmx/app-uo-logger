import { Service } from "typedi";
import { Notification } from "./interface";
import { EventEmitter } from 'events';
import { Character } from "../models/character";
import { CHARACTER_NOTIFIED } from "../constants/events";
import {NotificationEvent} from "../events/notification";

@Service()
export class NotificationManager {
    private registry: Map<string, Notification> = new Map();
    private state: Map<string, Map<string, number>> = new Map();

    /**
     * @param {EventEmitter} emitter
     */
    constructor(private emitter: EventEmitter) {

    }

    /**
     * @param {Notification | Notification[]} items
     */
    register(items: Notification | Notification[]) {
        (Array.isArray(items) ? items : [items]).forEach((item: Notification) => {
            this.registry.set(item.getName(), item);
        });
    }

    /**
     * @param {Character} character
     */
    async process(character: Character) {
        if (!this.state.has(character.getName())) {
            this.state.set(character.getName(), new Map());
        }

        this.registry.forEach((notification: Notification) => {
            if (
                !notification.isSatisfiedBy(character) ||
                this.isTooEarly(notification, character)
            ) {
                return;
            }

            this.emitter.emit(CHARACTER_NOTIFIED, <NotificationEvent>{
                character,
                notification,
            });

            this.state.get(character.getName()).set(notification.getName(), Date.now());
        });
    }

    /**
     * @param {Notification} notification
     * @param {Character} character
     */
    private isTooEarly(notification: Notification, character: Character): boolean {
        const state = this.state.get(character.getName());

        if (!state || !state.has(notification.getName())) {
            return false;
        }

        return Date.now() - state.get(notification.getName()) < (notification.getInterval() * 1000);
    }
}