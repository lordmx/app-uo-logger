import { Notification } from "./interface";
import { Character, CharacterProp } from "../models/character";
import { LOW_DURABILITY } from "../constants/notifications";

export const MIN_DURABILITY = 20;

export class LowDurability implements Notification {
    /**
     * @param {Character} character
     * @return {string}
     */
    formatMessage(character: Character): string {
        const payload = character.getPayload();
        const durabilities: Object = payload.get(CharacterProp.durabilityByLayers);

        if (!durabilities) {
            return null;
        }

        let layers: string[] = [];

        for (let layer in durabilities) {
            if (durabilities[layer] < MIN_DURABILITY) {
                layers.push(layer);
            }
        }

        if (!layers.length) {
            return null;
        }

        const layersStr: string = layers.map(item => `"${item}"`).join(', ');

        return `Character "${character.getName()}" has low durability in layers: ${layersStr}`;
    }

    /**
     * @return {number}
     */
    getInterval(): number {
        return 5 * 60;
    }

    /**
     * @return {string}
     */
    getName(): string {
        return LOW_DURABILITY;
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isSatisfiedBy(character: Character): boolean {
        const payload = character.getPayload();
        const durabilities: Object = payload.get(CharacterProp.durabilityByLayers);

        if (!durabilities) {
            return false;
        }

        for (let layer in durabilities) {
            if (durabilities[layer] < MIN_DURABILITY) {
                return true;
            }
        }

        return false;
    }

}