import { Character } from "../models/character";

export interface Notification {
    getName(): string;
    getInterval(): number;
    isSatisfiedBy(character: Character): boolean;
    formatMessage(character: Character): string;
}