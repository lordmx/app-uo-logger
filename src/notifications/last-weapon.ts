import { Notification } from "./interface";
import { Character, CharacterProp } from "../models/character";
import { LACK_OF_WEAPONS } from "../constants/notifications";

export const MIN_DURABILITY = 40;

export class LastWeapon implements Notification {
    /**
     * @param {Character} character
     * @return {string}
     */
    formatMessage(character: Character): string {
        const payload = character.getPayload();
        const durability = Number.parseInt(payload.get(CharacterProp.currentWeaponDurability));

        return `Character "${character.getName()}" has last weapon in hands and its durability is too low: ${durability}`;
    }

    /**
     * @return {number}
     */
    getInterval(): number {
        return 15 * 60;
    }

    /**
     * @return {string}
     */
    getName(): string {
        return LACK_OF_WEAPONS;
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isSatisfiedBy(character: Character): boolean {
        const payload = character.getPayload();
        const weaponsCount: number = Number.parseInt(payload.get(CharacterProp.remainderWeapons));
        const durability: number = Number.parseInt(payload.get(CharacterProp.currentWeaponDurability));

        return !character.isDead() && !weaponsCount && durability <= MIN_DURABILITY;
    }

}