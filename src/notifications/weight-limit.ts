import { Notification } from "./interface";
import { Character, CharacterProp } from "../models/character";
import { WEIGHT_LIMIT } from "../constants/notifications";

export const WEIGHT_PERCENT = 0.9;

export class WeightLimit implements Notification {
    /**
     * @param {Character} character
     * @return {string}
     */
    formatMessage(character: Character): string {
        const payload = character.getPayload();

        const weight: number = Number.parseInt(payload.get(CharacterProp.weight));
        const maxWeight: number = Number.parseInt(payload.get(CharacterProp.maxWeight));

        return `Character "${character.getName()}" is near to limit of weight: ${weight} / ${maxWeight}`;
    }

    /**
     * @return {number}
     */
    getInterval(): number {
        return 5 * 60;
    }

    /**
     * @return {string}
     */
    getName(): string {
        return WEIGHT_LIMIT;
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isSatisfiedBy(character: Character): boolean {
        const payload = character.getPayload();
        const weight: number = Number.parseInt(payload.get(CharacterProp.weight));
        const maxWeight: number = Number.parseInt(payload.get(CharacterProp.maxWeight));

        if (Number.isNaN(weight) || Number.isNaN(maxWeight)) {
            return false;
        }

        return Math.ceil(maxWeight * WEIGHT_PERCENT) <= weight;
    }

}