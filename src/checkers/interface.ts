import { Character } from "../models/character";

export interface Checker {
    getName(): string;
    isSatisfiedBy(character: Character): boolean;
    affect(character: Character);
}