import { Service } from "typedi";
import { Checker } from "./interface";
import { EventEmitter } from 'events';
import { Character } from "../models/character";
import { CharacterService } from "../services/character";
import { Pair } from "../infrastructure/pair";

export const CHECK_INTERVAL = 60;

@Service()
export class CheckerManager {
    private registry: Map<string, Checker> = new Map();

    /**
     * @param {EventEmitter} emitter
     * @param {CharacterService} characterService
     */
    constructor(
        private emitter: EventEmitter,
        private characterService: CharacterService,
    ) {
        setInterval(
            () => {
                characterService.getAll()
                    .then((result: Pair<Character[], number>) => {
                        const characters: Character[] = result.getA();
                        characters.forEach((character: Character) => {
                            this.check(character);
                        });
                    })
                    .catch((err) => {
                        console.error('Checker startup error: ', err);
                    });
            },
            CHECK_INTERVAL
        )


    }

    /**
     * @param {Checker | Checker[]} items
     */
    register(items: Checker | Checker[]) {
        (Array.isArray(items) ? items : [items]).forEach((item: Checker) => {
            this.registry.set(item.getName(), item);
        });
    }

    /**
     * @param {Character} character
     */
    async check(character: Character) {
        this.registry.forEach((checker: Checker) => {
            if (checker.isSatisfiedBy(character)) {
                checker.affect(character);
            }
        });
    }
}