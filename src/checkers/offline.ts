import {Checker} from "./interface";
import {CHECK_OFFLINE} from "../constants/checkers";
import {Character, CharacterStatus} from "../models/character";
import {Container} from "typedi";
import {CharacterService} from "../services/character";

export class OfflineChecker implements Checker {
    private characterService: CharacterService;

    constructor() {
        this.characterService = Container.get(CharacterService);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return CHECK_OFFLINE;
    }

    /**
     * @param {Character} character
     * @return {boolean}
     */
    isSatisfiedBy(character: Character): boolean {
        return character.getStatus() != CharacterStatus.offline && character.isOffline();
    }

    /**
     * @param {Character} character
     * @return {string}
     */
    formatMessage(character: Character): string {
        return `Character "${character.getName()}" is offline now`;
    }

    /**
     * @param {Character} character
     */
    affect(character: Character) {
        this.characterService.setStatus(character, CharacterStatus.offline);
    }
}