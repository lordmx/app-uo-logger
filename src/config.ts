import { Service } from "typedi/decorators/Service";

@Service()
export class Config {
    host: string = process.env.HOST;
    port: number = Number.parseInt(process.env.PORT);
    socketPort: number = Number.parseInt(process.env.SOCKET_PORT);
    bus: ServiceBusOpt = new ServiceBusOpt();
    filePath: string = process.env.FILE_PATH;
    mongo = {
        dsn: process.env.MONGODB_DSN,
    };
    tempPath: string = process.env.TMP_PATH || '/tmp';
    discord = {
        token: process.env.DISCORD_TOKEN,
        prefix: process.env.DISCORD_PREFIX || '+',
        channel: process.env.DISCORD_CHANNEL,
    };
}

class ServiceBusOpt {
    public nodeID: string = process.env.SB_NODE_ID || null;
    public namespace: string = process.env.SB_NAMESPACE || '';
    public transporter: string = process.env.SB_TRANSPORTER || 'AMQP';
    public cacher: string = process.env.SB_CACHER || 'memory';
    public serializer: string = process.env.SB_SERIALIZER || 'JSON';
    public logger: string | boolean = process.env.SB_LOGGER || true;
    public logLevel: string = process.env.SB_LOG_LEVEL || 'info';
    public logFormatter: string = process.env.SB_LOG_FORMATTER || 'default';
    public registryStrategy: string = process.env.SB_REGISTRY_STRATEGY || 'RoundRobin';
    public requestTimeout: number = process.env.SB_REQUEST_TIMEOUT || 10000;
    public requestRetryCount: number = process.env.SB_REQUEST_RETRY_COUNT || 0;
    public requestRetryDelay: number = process.env.SB_REQUEST_RETRY_DELAY || 0;
    public requestMaxRetryDelay: number = process.env.SB_REQUEST_RETRY_MAX_DELAY || 0;
    public maxCallLevel: number = process.env.SB_MAX_CALL_LEVEL || 100;
    public heartbeatInterval: number = process.env.SB_HEARTBEAT_INTERVAL || 5;
    public heartbeatTimeout: number = process.env.SB_HEARTBEAT_TIMEOUT || 15;
    public circularBreakerMaxFailures: number = process.env.SB_CIRCULAR_BREAKER_MAX_FAILURES || 3;
}