import { Message } from "discord.js";

export interface Command {
    getName(): string;
    execute(message: Message, args: any[]);
}