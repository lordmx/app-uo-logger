import { Container, Service } from "typedi";
import { Command } from "./interface";
import { Message } from "discord.js";

@Service()
export class CommandManager {
    private registry: Map<string, Command> = new Map();

    /**
     * @param {Command | Command[]} items
     */
    register(items: Command | Command[]) {
        (Array.isArray(items) ? items : [items]).forEach((item: Command) => {
            this.registry.set(item.getName(), item);
        });
    }

    /**
     * @param {string} command
     * @param {Message} message
     * @param {any[]} args
     */
    async execute(command: string, message: Message, args: any[]) {
        if (!this.registry.has(command)) {
            message = <Message>await message.channel.send("Status?");
            message.edit('Wrong command');
            return;
        }

        this.registry.get(command).execute(message, args);
    }
}

let commandManager = new CommandManager();
Container.set(CommandManager, commandManager);

export {
    commandManager,
}