import * as commands from '../constants/commands';
import { Command } from "./interface";
import { Message } from "discord.js";
import { Character } from "../models/character";
import { Container } from "typedi";
import { CharacterService } from "../services/character";
import { SocketManager } from "../infrastructure/sockets";

export class PictureCommand implements Command {
    private characterService: CharacterService;
    private socketManager: SocketManager;

    constructor() {
        this.characterService = Container.get(CharacterService);
        this.socketManager = Container.get(SocketManager);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return commands.PICTURE;
    }

    /**
     * @param {Message} message
     * @param {any[]} args
     */
    async execute(message: Message, args: string[]) {
        message = <Message>await message.channel.send("Picture?");

        if (!args.length) {
            return message.edit(`Characters names were not given`);
        }

        await this.characterService.get(args[0])
            .then((character: Character) => {
                if (!character) {
                    return message.edit(`Characters not found`);
                }

                if (!this.socketManager.isRegistered(character)) {
                    return message.edit(`Characters did not found connect to the server`);
                }

                message.edit('Request sent');
                this.socketManager.send(character, {
                    'operation': 'picture',
                });


            })
            .catch((err) => {
                message.edit(`Fetching error: ${err.message}`);
                console.error('[Discord] Error: ', err);
            });
    }
}