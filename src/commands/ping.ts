import { Command } from "./interface";
import { Message } from "discord.js";
import * as commands from '../constants/commands';

export class PingCommand implements Command {
    /**
     * @return {string}
     */
    getName(): string {
        return commands.PING;
    }

    /**
     * @param {Message} message
     */
    async execute(message: Message) {
        const result: Message = <Message>await message.channel.send("Ping?");
        result.edit(
            `Pong! Latency is ${result.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(message.client.ping)}ms`
        );
    }
}