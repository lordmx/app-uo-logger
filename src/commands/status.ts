import * as commands from '../constants/commands';
import { Command } from "./interface";
import { Message } from "discord.js";
import { Character } from "../models/character";
import { Container } from "typedi";
import { CharacterService } from "../services/character";
import { QueryFilterOpt } from "../infrastructure/query";

export class StatusCommand implements Command {
    private characterService: CharacterService;

    constructor() {
        this.characterService = Container.get(CharacterService);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return commands.STATUS;
    }

    /**
     * @param {Message} message
     * @param {any[]} args
     */
    async execute(message: Message, args: string[]) {
        message = <Message>await message.channel.send("Status?");

        if (!args.length) {
            return message.edit(`Characters names were not given`);
        }

        if (args.length === 1 && args[0] == 'all') {
            const all: Character[] = (await this.characterService
                .getAll(new QueryFilterOpt(), null))
                .getA();

            args = all.map(item => item.getName());
        }

        await this.characterService.get(args)
            .then((characters: Character[]) => {
                if (!characters.length) {
                    return message.edit(`Characters not found`);
                }

                message.edit('Status fetched');

                characters.forEach((character: Character) => {
                    message.channel.send(
                        `${character.getName()}: ${character.getStatusAsString()}, last at ${character.getLastActivityDateAsString()}\n\`\`\`${character.getPayloadFormatted()}\`\`\``
                    );
                });
            })
            .catch((err) => {
                message.edit(`Fetching error: ${err.message}`);
                console.error('[Discord] Error: ', err);
            });
    }
}