import * as commands from '../constants/commands';
import { Command } from "./interface";
import { Message } from "discord.js";
import { Character } from "../models/character";
import { Loot } from "../models/loot";
import { Container } from "typedi";
import { CharacterService } from "../services/character";
import { LootService } from "../services/loot";
import { Config } from "../config";
import * as fs from 'fs';

export class LootCommand implements Command {
    private characterService: CharacterService;
    private lootService: LootService;
    private config: Config;

    constructor() {
        this.characterService = Container.get(CharacterService);
        this.lootService = Container.get(LootService);
        this.config = Container.get(Config);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return commands.LOOT;
    }

    /**
     * @param {Message} message
     * @param {any[]} args
     */
    async execute(message: Message, args: string[]) {
        message = <Message>await message.channel.send("Loot?");

        const name = args[0];

        if (!name) {
            return message.edit(`Characters name was not given`);
        }

        await this.characterService.get(name)
            .then((character: Character) => {
                if (!character) {
                    return message.edit(`Character not found`);
                }

                const limit = Math.min(100, Number.parseInt(args[1]) || 10);

                this.lootService.getByCharacter(character, limit)
                    .then(async (items: Loot[]) => {
                        message.edit(`Fetching items (${limit}, max: 100)...`);

                        let attachment = <Message>await message.channel.send("Attachment?");

                        if (!items.length) {
                            return attachment.edit('<Empty loot log>');
                        }

                        let content: string[] = [];

                        items.forEach((item: Loot) => {
                            content.push(
                                `[${item.created_at.toLocaleString()}] ${item.getName()}:\n${item.getProperties().join('\n')}`
                            );
                        });

                        let path = `${this.config.tempPath}/${character.getName()}_${Date.now()}_${limit}.txt`;
                        fs.writeFile(path, content.join('\n\n'), (err) => {
                             if (err) {
                                 console.error('Attach file error: ', err);
                                 return attachment.edit('Attach file error: ' + err.message);
                             }

                             attachment.edit('File attached');
                             attachment.channel.sendFile(path)
                                 .then(() => {
                                     fs.unlink(path, (err) => {

                                     });
                                 })
                                 .catch((err) => {
                                     console.error('Attach file error: ', err);
                                     return attachment.edit('Attach file error: ' + err.message);
                                 });
                        });
                    })
                    .catch((err) => {
                        message.edit(`Fetching error: ${err.message}`);
                        console.error('[Discord] Error: ', err);
                    });

            })
            .catch((err) => {
                message.edit(`Fetching error: ${err.message}`);
                console.error('[Discord] Error: ', err);
            });
    }
}