import * as commands from '../constants/commands';
import { Command } from "./interface";
import { Message } from "discord.js";
import { Character } from "../models/character";
import { Container } from "typedi";
import { CharacterService } from "../services/character";
import { QueryFilterOpt} from "../infrastructure/query";
import { Pair } from "../infrastructure/pair";

export class ListCommand implements Command {
    private characterService: CharacterService;

    constructor() {
        this.characterService = Container.get(CharacterService);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return commands.LIST;
    }

    /**
     * @param {Message} message
     */
    async execute(message: Message) {
        message = <Message>await message.channel.send("List?");

        const opt = new QueryFilterOpt();
        opt.sort = 'status,-last_activity_date';

        await this.characterService.getAll(opt, null)
            .then((result: Pair<Character[], number>) => {
                if (!result.getB()) {
                    return message.edit('Fetching result: <empty list>');
                }

                let content: string = 'Characters: \n';

                result.getA().forEach((character: Character) => {
                    content += `[${character.getLastActivityDateAsString()}] ${character.getName()}: ${character.getStatusAsString()}\n`;
                });

                message.edit(content);
            })
            .catch((err) => {
                message.edit(`Fetching error: ${err.message}`);
                console.error('[Discord] Error: ', err);
            });
    }
}