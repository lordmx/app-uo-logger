import {
    SocketController,
    OnConnect,
    OnDisconnect,
    ConnectedSocket
} from "socket-controllers";
import { Service } from "typedi";
import { SocketManager } from "../infrastructure/sockets";
import { Socket } from "socket.io";
import { CharacterService } from "../services/character";
import { Character } from "../models/character";

@SocketController()
@Service()
export class RpcController {
    private characters: Map<string, Character> = new Map();

    constructor(
        private socketManager: SocketManager,
        private characterService: CharacterService,
    ) {

    }

    @OnConnect()
    async onConnect(@ConnectedSocket() socket: Socket) {
        const character: Character = await this.getCharacter(socket)
            .catch(() => {
                return null;
            });

        if (character) {
            this.socketManager.register(character, socket);
            console.info(`Character "${character.getName()}" connected`);
        }
    }

    @OnDisconnect()
    async onDisconnect(@ConnectedSocket() socket: Socket) {
        const character: Character = await this.getCharacter(socket)
            .catch(() => {
                return null;
            });

        if (character) {
            this.socketManager.register(character, socket);
            console.info(`Character "${character.getName()}" disconnected`);
        }
    }

    /**
     * @param {Socket} socket
     * @return {Promise<Character>}
     */
    private async getCharacter(socket: Socket): Promise<Character> {
        const name: string = (socket.handshake.query.params || {})['character'];

        if (!name) {
            return Promise.resolve(null);
        }

        if (this.characters.has(name)) {
            return Promise.resolve(this.characters.get(name));
        }

        return new Promise<Character>((resolve, reject) => {
            this.characterService.get(name)
                .then((character: Character) => {
                    this.characters.set(name, character);
                    resolve(character);
                })
                .catch((err) => {
                    console.error('Fetch character error: ', err);
                    reject(err);
                })
        });
    }

}