import { Service } from 'typedi';
import { EventEmitter } from 'events';
import { StatusChangedEvent } from "../events/change-status";
import { Discord } from "../infrastructure/discord";
import { Config } from "../config";
import { LootReceivedEvent } from "../events/loot-received";
import { NotificationEvent } from "../events/notification";

@Service()
export class CharacterListener {
    /**
     * @param {EventEmitter} emitter
     * @param {Discord} discord
     * @param {Config} config
     */
    constructor(
        private emitter: EventEmitter,
        private discord: Discord,
        private config: Config,
    ) {

    }

    /**
     * @param {StatusChangedEvent} event
     */
    onStatusChanged(event: StatusChangedEvent) {
        this.discord.send(
            this.config.discord.channel,
            `Character "${event.character.getName()}" changed status from "${(event.oldStatus || 'N/A').toString()}" to "${event.newStatus.toString()}"`,
        );
    }

    /**
     * @param {StatusChangedEvent} event
     */
    onLootReceived(event: LootReceivedEvent) {
        this.discord.send(
            this.config.discord.channel,
            `Character "${event.character.getName()}" picked up "${event.loot.getName()}":\n\`\`\`${event.loot.getProperties().join("\n")}\`\`\``
        );
    }

    /**
     * @param {NotificationEvent} event
     */
    onNotificationReceived(event: NotificationEvent) {
        const message: string = event.notification.formatMessage(event.character);

        if (message) {
            this.discord.send(this.config.discord.channel, message);
        }
    }
}