import { Service } from "typedi";
import { Action } from "routing-controllers";
import { Context } from "koa";
import { User } from '../models/user';
import { FindConditions, getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories/user";

@Service()
export class UserService {
    /**
     * @param {string} token
     * @return {Promise<Character>}
     */
    static async getByToken(token: string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            getCustomRepository(UserRepository).find(<FindConditions<User>>{
                where: { token },
            })
                .then((res: User[]) => {
                    if (!res.length) {
                        return resolve(null);
                    }

                    resolve(res[0]);
                })
                .catch(reject);
        });
    }

    /**
     * @param {Action} action
     * @return {Promise<User>}
     */
    static async currentUserChecker(action: Action): Promise<User> {
        let token: string;
        const context: Context = <Context>action.context;

        if (context.headers['authorization']) {
            let matches = (context.headers['authorization'] || '').split('Bearer ');
            matches = matches
                .map(item => item.trim())
                .filter(item => item);

            if (matches && matches.length > 0) {
                token = matches[0];
            }
        } else if (context.query['access_token']) {
            token = context.query['access_token'];
        }

        if (!token) {
            return Promise.resolve(null);
        }

        console.info('Authorization token: ', token);

        return UserService.getByToken(token);
    }

    /**
     * @param {Action} action
     * @return {Promise<boolean>}
     */
    static async authorizationChecked(action: Action): Promise<boolean> {
        const currentUser: User = await UserService.currentUserChecker(action)
            .catch((err) => {
                return Promise.reject(err);
            });

        return Promise.resolve(currentUser !== null);
    }
}