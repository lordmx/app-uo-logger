import { Service} from "typedi";
import { CharacterRepository} from "../repositories/character";
import {
    Character,
    CharacterOpt,
    CharacterProp,
    CharacterStatus,
} from "../models/character";
import {
    FindConditions,
    FindManyOptions,
    getCustomRepository,
} from "typeorm";
import { QueryFilterOpt } from "../infrastructure/query";
import { Pair } from "../infrastructure/pair";
import { mapToObject } from "../infrastructure/utils";
import { StatusChangedEvent } from "../events/change-status";
import { CHARACTER_STATUS_CHANGED } from "../constants/events";
import { EventEmitter } from 'events';
import { NotificationManager } from "../notifications/manager";

@Service()
export class CharacterService {
    private characterRepo: CharacterRepository;

    /**
     * @param {EventEmitter} emitter
     * @param {NotificationManager} notificationManager
     */
    constructor(
        private emitter: EventEmitter,
        private notificationManager: NotificationManager,
    ) {
        this.characterRepo = getCustomRepository(CharacterRepository);
    }

    /**
     * @param {Character} character
     * @param {CharacterStatus} newStatus
     * @param {Map<CharacterProp, any>} payload
     * @return {Promise<Character>}
     */
    public setStatus(
        character: Character,
        newStatus: CharacterStatus,
        payload?: Map<CharacterProp, any>,
    ): Promise<Character> {
        const oldStatus: CharacterStatus = character.getStatus();

        character.setStatus(newStatus);

        if (payload) {
            character.setPayload(payload);
        }

        return new Promise<Character>((resolve, reject) => {
            this.characterRepo.save(character)
                .then(() => {
                    if (oldStatus != newStatus) {
                        this.emitter.emit(
                            CHARACTER_STATUS_CHANGED,
                            <StatusChangedEvent>{character, newStatus, oldStatus},
                        );
                    }

                    this.notificationManager.process(character);

                    resolve(character);
                })
                .catch(reject);
        });
    }

    /**
     * @param {string | string[]} name
     * @return {Promise<Character | Character[]>}
     */
    public get(name: string | string[]): Promise<Character | Character[]> {
        return new Promise<Character | Character[]>((resolve, reject) => {
            const names: string[] = (Array.isArray(name) ? name : [name])
                .map(item => item.trim())
                .filter(item => item);

            this.characterRepo.find(<FindConditions<Character>>{
                where: { name: { '$in': names }},
            })
                .then((res: Character[]) => {
                    if (!res.length) {
                        return resolve(Array.isArray(name) ? [] : null);
                    }

                    resolve(Array.isArray(name) ? res : res[0]);
                })
                .catch(reject);
        });
    }

    /**
     * @param {string} id
     * @return {Promise<Character>}
     */
    public getById(id: string): Promise<Character> {
        return this.characterRepo.findOne(id);
    }

    /**
     * @param {string} name
     * @return {Promise<Character>}
     */
    public create(name: string): Promise<Character> {
        return new Promise<Character>(async (resolve, reject) => {
            const exists: Character = await this.get(name).catch((err) => {
                reject(err);
                return null;
            });

            if (exists) {
                return resolve(exists);
            }

            const opt: CharacterOpt = { name, status: null };
            let character = new Character(opt);

            this.characterRepo.save(character)
                .then(resolve)
                .catch(reject);

        });
    }

    /**
     * @param {QueryFilterOpt} opt
     * @param {<T>} criteria
     * @return {Promise<[Character[], number]>}
     */
    public getAll<T extends Character>(opt?: QueryFilterOpt, criteria?: T): Promise<Pair<Character[], number>> {
        opt = opt || new QueryFilterOpt();

        return new Promise<Pair<Character[], number>>((resolve, reject) => {
            this.characterRepo.findAndCount(<FindManyOptions>{
                select: opt.getFields(),
                where: criteria || <T>{},
                order: <any>mapToObject(opt.getSort()),
                skip: opt.getOffset(),
                take: opt.getLimit(),
            })
                .then((res: [Character[], number]) => {
                    resolve(Pair.fromArray(res));
                })
                .catch(reject);
        });
    }
}
