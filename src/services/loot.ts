import { Service } from "typedi";
import { LootRepository } from "../repositories/loot";
import { Loot, LootOpt } from "../models/loot";
import { Character } from '../models/character';
import { FindManyOptions, getCustomRepository } from "typeorm";
import { QueryFilterOpt } from "../infrastructure/query";
import { Pair } from "../infrastructure/pair";
import { mapToObject } from "../infrastructure/utils";
import { LootReceivedEvent } from "../events/loot-received";
import { CHARACTER_LOOT_RECEIVED } from "../constants/events";
import { EventEmitter } from 'events';

@Service()
export class LootService {
    private lootRepo: LootRepository;

    /**
     * @param {EventEmitter} emitter
     */
    constructor(private emitter: EventEmitter) {
        this.lootRepo = getCustomRepository(LootRepository);
    }

    /**
     * @param {Character} character
     * @param {string} name
     * @param {string[]} properties
     * @param {Object} meta
     * @return {Promise<Loot>}
     */
    public async create(
        character: Character,
        name: string,
        properties: string[],
        meta?: Object
    ): Promise<Loot> {
        if (!name) {
            name = properties[0];
            properties.splice(0, 1);
        }

        properties = properties.filter(item => item != '<b>Insured</b>');
        properties = properties.filter(item => item && item[0] != '*');

        const exists: Loot[] = await this.getByProperties(name, properties)
            .catch((err) => {
                console.error('Check properties error: ', err);
                return [];
            });

        if (exists.length > 0) {
            console.info('Loot with same properties already exists');
            return Promise.resolve(exists[0]);
        }

        const opt: LootOpt = {
            character: character.getObjectId(),
            name,
            properties,
            meta: meta || {},
        };

        return new Promise<Loot>((resolve, reject) => {
            let loot = new Loot(opt);
            loot.created_at = new Date();

            this.lootRepo.save(loot)
                .then((loot: Loot) => {
                    this.emitter.emit(CHARACTER_LOOT_RECEIVED, <LootReceivedEvent>{
                        character,
                        loot,
                    });

                    resolve(loot);
                })
                .catch(reject);
        });
    }

    /**
     * @param {Character} character
     * @param {Number} limit
     * @return {Promise<Loot[]>}
     */
    public getByCharacter(character: Character, limit: number): Promise<Loot[]> {
        return this.lootRepo.find(<FindManyOptions>{
            where: { character: character.getObjectId() },
            order: { created_at: -1 },
            take: limit,
        });
    }

    /**
     * @param {string} name
     * @param {string[]} properties
     * @return {Promise<Loot[]>}
     */
    public getByProperties(name: string, properties: string[]): Promise<Loot[]> {
        return this.lootRepo.find(<FindManyOptions>{
            where: { name, properties_hash: Loot.getPropertiesHash(properties) },
        });
    }

    /**
     * @param {QueryFilterOpt} opt
     * @param {<T>} criteria
     * @return {Promise<[Character[], number]>}
     */
    public getAll<T extends Loot>(opt: QueryFilterOpt, criteria: T): Promise<Pair<Loot[], number>> {
        return new Promise<Pair<Loot[], number>>((resolve, reject) => {
            this.lootRepo.findAndCount(<FindManyOptions>{
                select: opt.getFields(),
                where: criteria,
                order: <any>mapToObject(opt.getSort()),
                skip: opt.getOffset(),
                take: opt.getLimit(),
            })
                .then((res: [Loot[], number]) => {
                    resolve(Pair.fromArray(res));
                })
                .catch(reject);
        });
    }
}
