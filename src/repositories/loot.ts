import { EntityRepository, Repository } from "typeorm";
import { Loot } from "../models/loot";

@EntityRepository(Loot)
export class LootRepository extends Repository<Loot> {

}