import { EntityRepository, Repository } from "typeorm";
import { Character } from "../models/character";

@EntityRepository(Character)
export class CharacterRepository extends Repository<Character> {

}