import { Action, Interceptor, InterceptorInterface } from "routing-controllers";
import { Pair } from "../infrastructure/pair";
import { QueryFilterOpt } from "../infrastructure/query";
import { ResultSet } from "../infrastructure/result";

type Arg = Pair<any, any> | Array<any> | { toJSON: Function };

@Interceptor()
export class JsonStringifyInterceptor implements InterceptorInterface {
    /**
     * @param {Action} action
     * @param {Arg} result
     */
    intercept(action: Action, result: Arg): any | Promise<any> {
        if (result instanceof Pair) {
            const opt = QueryFilterOpt.fromContext(action.context);
            result = new ResultSet<any>(result.getA(), result.getB(), opt);
        }

        if (Array.isArray(result)) {
            return result.map(item => {
                if ('toJSON' in item) {
                    return item.toJSON();
                }

                return item;
            });
        } else if ('toJSON' in result) {
            return result.toJSON();
        }

        return result;
    }
}