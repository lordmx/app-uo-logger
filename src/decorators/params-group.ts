import {Action, createParamDecorator} from "routing-controllers";

export function ParamsGroup<T>(type: { new(): T }) {
    return createParamDecorator({
        required: false,
        value: (action: Action) => {
            let value: T = new type();

            Object.getOwnPropertyNames(value).forEach((key: string) => {
                Reflect.set(value as Object, key, action.context.query[key]);
            });

            return value;
        }
    });
}