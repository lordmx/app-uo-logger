import {
    JsonController,
    Get,
    Post,
    CurrentUser,
    QueryParam,
    Authorized,
    BodyParam,
    Ctx,
    NotFoundError,
    BadRequestError,
    InternalServerError,
} from "routing-controllers";
import { Service } from "typedi";
import { Loot } from "../models/loot";
import { Character } from "../models/character";
import { LootService } from "../services/loot";
import { QueryFilterOpt } from "../infrastructure/query";
import { Pair } from "../infrastructure/pair";
import { ParamsGroup } from "../decorators/params-group";
import { User } from "../models/user";
import { CharacterService } from "../services/character";
import { Context } from "koa";

class Criteria {
    @QueryParam('character', { required: true })
    character: string | Character;
}

@Service()
@JsonController()
export class LootController {
    /**
     * @param {LootService} lootService
     * @param {CharacterService} characterService
     */
    constructor(
        private lootService: LootService,
        private characterService: CharacterService,
    ) {

    }

    /**
     * @api {get} /loot/ Get loot list
     * @apiVersion 1.0.0
     * @apiName LootList
     * @apiGroup Loot
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/loot/?character=5be20b9069078c639c381468 -X GET
     *
     * @apiUse HeaderContentType
     * @apiUse Loot
     * @apiUse LootExample
     *
     * @apiParam {String} character Character ID (required)
     *
     * @apiHeader {String} Authorization JWT Token
     * @apiHeaderExample {json} AuthorizationExample:
     * {
     *     "Authorization": "Bearer <token>"
     * }
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {QueryFilterOpt} filters
     * @param {Criteria} criteria
     * @param {User} user
     */
    @Get("/loot")
    all(
        @ParamsGroup(QueryFilterOpt) filters: QueryFilterOpt,
        @ParamsGroup(Criteria) criteria: Criteria,
        @CurrentUser({ required: true }) user: User,
    ): Promise<Pair<Loot[], number>> {
        return this.lootService.getAll(filters, <Loot>criteria);
    }

    /**
     * @api {post} /loot Create loot's log
     * @apiVersion 1.0.0
     * @apiName LootCreate
     * @apiGroup Loot
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/loot -X POST
     *
     * @apiUse HeaderContentType
     * @apiUse Loot
     * @apiUse LooExample
     *
     * @apiHeader {String} Authorization JWT Token
     * @apiHeaderExample {json} AuthorizationExample:
     * {
     *     "Authorization": "Bearer <token>"
     * }
     *
     * @apiParam {string} character
     * @apiParam {String} name
     * @apiParam {String} properties
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {String} character
     * @param {String} name
     * @param {String} properties
     * @param {Object} meta
     * @param {Context} ctx
     */
    @Post("/loot")
    @Authorized()
    create(
        @BodyParam('character') character: string,
        @BodyParam('name') name: string,
        @BodyParam('properties') properties: string | string[],
        @BodyParam('meta') meta: object,
        @Ctx() ctx: Context,
    ): Promise<Loot> {
        return new Promise<Loot>((resolve, reject) => {
            this.characterService.get(character)
                .then((character: Character) => {
                    if (!character) {
                        return reject(new NotFoundError('Character not found'));
                    }

                    this.lootService.create(
                        character,
                        name,
                        Array.isArray(properties)
                            ? properties
                            : properties.split('\n'),
                        meta || {},
                    )
                        .then((result: Loot) => {
                            ctx.status = 201;
                            resolve(result);
                        })
                        .catch((err) => {
                            reject(new BadRequestError(err));
                        });
                })
                .catch((err) => {
                    reject(new InternalServerError(err))
                });
        });
    }
}