import {
    JsonController,
    Get,
    Post,
    Param,
    Body,
    Authorized,
    UploadedFile,
    Ctx,
    BadRequestError,
} from "routing-controllers";
import { Service } from "typedi";
import { Character } from "../models/character";
import { CharacterService } from "../services/character";
import { QueryFilterOpt } from "../infrastructure/query";
import { Pair } from "../infrastructure/pair";
import { ParamsGroup } from "../decorators/params-group";
import * as multer from "multer";
import { extname } from "path";
import { Container } from "typedi";
import { Config } from "../config";
import { Request, Context } from "koa";

class Criteria {
    name: string;
}

type MulterFile = {
    originalname: string;
};

const uploadOpt = (): multer.Options => {
    return {
        storage: multer.diskStorage({
            destination: () => {
                const config: Config = Container.get(Config);
                return config.filePath;
            },
            filename: (req: any, file: MulterFile, cb: Function) => {
                const id: string = (<Request>req).ctx.param('id');

                const characterService = Container.get(CharacterService);
                characterService.getById(id)
                    .then((character: Character) => {
                        if (!character) {
                            return cb(new Error('Character not found'), false);
                        }

                        const ext: string = extname(file.originalname);
                        const filename: string = `${character.getName()}${ext}`;

                        cb(filename, true);
                    })
                    .catch(() => {
                        cb(new Error('Could not get character by id'), false);
                    });
            },
        }),
        fileFilter: (req: any, file: MulterFile, cb: Function) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                return cb(new Error('Only image files are allowed!'), false);
            }
            cb(null, true);
        },
        limits: {
            fieldNameSize: 255,
            fileSize: 1024 * 1024 * 2
        }
    };
};

@Service()
@JsonController()
export class CharactersController {
    /**
     * @param {CharacterService} characterService
     */
    constructor(private characterService: CharacterService) {

    }

    /**
     * @api {get} /characters/ Get characters list
     * @apiVersion 1.0.0
     * @apiName CharactersList
     * @apiGroup Characters
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/characters/ -X GET
     *
     * @apiUse HeaderContentType
     * @apiUse Character
     * @apiUse CharacterExample
     *
     * @apiHeader {String} Authorization JWT Token
     * @apiHeaderExample {json} AuthorizationExample:
     * {
     *     "Authorization": "Bearer <token>"
     * }
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {QueryFilterOpt} filters
     * @param {Criteria} criteria
     */
    @Get("/characters")
    @Authorized()
    all(
         @ParamsGroup(QueryFilterOpt) filters: QueryFilterOpt,
         @ParamsGroup(Criteria) criteria: Criteria
    ): Promise<Pair<Character[], number>> {
        return this.characterService.getAll(filters, <Character>criteria);
    }

    /**
     * @api {get} /characters/:id Get character by ID
     * @apiVersion 1.0.0
     * @apiName CharactersView
     * @apiGroup Characters
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/characters/5be20b9069078c639c381468 -X GET
     *
     * @apiUse HeaderContentType
     * @apiUse Character
     * @apiUse CharacterExample
     *
     * @apiHeader {String} Authorization JWT Token
     * @apiHeaderExample {json} AuthorizationExample:
     * {
     *     "Authorization": "Bearer <token>"
     * }
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {string} id
     */
    @Get("/characters/:id")
    @Authorized()
    one(
        @Param('id') id: string,
    ): Promise<Character> {
        return this.characterService.getById(id);
    }

    /**
     * @api {post} /characters/:id/picture Upload picture
     * @apiVersion 1.0.0
     * @apiName CharactersUpsert
     * @apiGroup Characters
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/characters/5be20b9069078c639c381468/picture -X POST
     *
     * @apiUse Character
     * @apiUse CharacterExample
     *
     * @apiHeader {String} Authorization JWT Token
     * @apiHeaderExample {json} AuthorizationExample:
     * {
     *     "Authorization": "Bearer <token>"
     * }
     *
     * @apiHeader {String} Content-type
     * @apiHeaderExample {json} ContentTypeExample:
     * {
     *     "Content-type": "multipart/form-data"
     * }
     *
     * @apiParam {String} id
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {string} id
     * @param {any} picture
     */
    @Post("/characters/:id/picture")
    @Authorized()
    picture(
        @Param('id') id: string,
        @UploadedFile('picture', { options: uploadOpt() }) picture: any,
    ): Promise<Character> {
        return this.characterService.getById(id);
    }

    /**
     * @api {post} /characters Upsert character's data
     * @apiVersion 1.0.0
     * @apiName CharactersUpsert
     * @apiGroup Characters
     * @apiPermission none
     *
     * @apiExample Example:
     * curl -i http://localhost/api/v1/characters -X POST
     *
     * @apiUse HeaderContentType
     * @apiUse Character
     * @apiUse CharacterExample
     *
     *
     * @apiHeader {String} Content-type
     * @apiHeaderExample {json} ContentTypeExample:
     * {
     *     "Content-type": "application/json"
     * }
     *
     * @apiParam {String} name
     * @apiParam {String} status
     * @apiParam {Object} payload
     *
     * @apiUse Error400
     * @apiUse Error401
     * @apiUse Error500
     *
     * @param {Character} oldCharacter
     * @param {Context} ctx
     */
    @Post("/characters")
    @Authorized()
    create(
        @Body() oldCharacter: Character,
        @Ctx() ctx: Context,
    ): Promise<Character> {
        return new Promise<Character>((resolve, reject) => {
            this.characterService.create(oldCharacter.getName())
                .then((newCharacter: Character) => {
                    this.characterService.setStatus(
                        newCharacter,
                        oldCharacter.getStatus(),
                        oldCharacter.getPayload(),
                    )
                        .then((result: Character) => {
                            ctx.status = 201;
                            resolve(result);
                        })
                        .catch((err) => {
                            reject(new BadRequestError(err));
                        });
                })
                .catch((err) => {
                    reject(new BadRequestError(err));
                });
        });
    }
}