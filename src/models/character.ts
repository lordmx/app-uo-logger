import {
    Entity,
    ObjectID,
    ObjectIdColumn,
    Column,
    Index,
} from "typeorm";
import { mapToObject, objectToMap } from "../infrastructure/utils";
import { BodyParam } from "routing-controllers";

export enum CharacterStatus {
    idle = 'idle',
    farming = 'farming',
    dead = 'dead',
    offline = 'offline',
}

export interface CharacterOpt {
    name: string;
    status: CharacterStatus;
}

export enum CharacterProp {
    weight = 'weight',
    maxWeight = 'maxWeight',
    luck = 'luck',
    tithingPoints = 'tithingPoints',
    remainderWeapons = 'remainderWeapons',
    remainderShields = 'remainderShields',
    hasShield = 'hasShield',
    currentWeaponDurability = 'currentWeaponDurability',
    currentShieldDurability = 'currentShieldDurability',
    lootedItemsCount = 'lootedItemsCount',
    durabilityByLayers = 'durabilityByLayers',
}

@Entity({
    name: 'characters'
})
export class Character {
    
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    @Index()
    @BodyParam('name', { required: true })
    name: string;

    @Column()
    @Index()
    status: CharacterStatus;

    @Column({ default: Date.now })
    last_activity_date: Date;

    @Column({ default: {} })
    payload: Map<CharacterProp, any>;

    /**
     * @return {CharacterOpt}
     */
    constructor(data: CharacterOpt) {
        Object.assign(this, data);
    }

    /**
     * @return {string}
     */
    getId(): string {
        return this._id ? this._id.toString() : null;
    }

    /**
     * @return {ObjectID}
     */
    getObjectId(): ObjectID {
        return this._id;
    }

    /**
     * @return {string}
     */
    getName(): string {
        return this.name;
    }

    /**
     * @return {String}
     */
    getPayloadFormatted(): string {
        const payload = this.getPayload();
        let formattedDurabilities: string[] = [];

        const durabilities: object = payload.get(CharacterProp.durabilityByLayers) || {};
        Object.keys(durabilities).forEach((key: string) => {
            formattedDurabilities.push(`   Layer "${key}": ${durabilities[key]}`);
        });

        let parts: string[] = [
            `Luck: ${payload.get(CharacterProp.luck) || 'N/A'}`,
            `Weight: ${payload.get(CharacterProp.weight) || 'N/A'} / ${payload.get(CharacterProp.maxWeight) || 'N/A'}`,
            `Tithing points: ${payload.get(CharacterProp.tithingPoints) || 'N/A'}`,
            `Weapons (remains, current durability): ${payload.get(CharacterProp.remainderWeapons) || 'N/A'}, ${payload.get(CharacterProp.currentWeaponDurability) || 'N/A'}`,
            `Shields (equipped, remains, current durability): ${payload.get(CharacterProp.hasShield) ? 'yes' : 'no'}, ${payload.get(CharacterProp.remainderShields) || 'N/A'}, ${payload.get(CharacterProp.currentShieldDurability) || 'N/A'}`,
            `Looted items: ${payload.get(CharacterProp.lootedItemsCount) || 'N/A'}`,
        ];


        if (formattedDurabilities.length > 0) {
            parts.push(`Durability by layers:\n${formattedDurabilities.join('\n')}`);
        }

        return parts.join('\n');
    }

    /**
     * @param {CharacterStatus} status
     */
    setStatus(status: CharacterStatus) {
        this.status = status;
        this.last_activity_date = new Date();
    }

    /**
     * @return {string}
     */
    getLastActivityDateAsString(): string {
        return this.last_activity_date
            ? this.last_activity_date.toLocaleString()
            : 'N/A';
    }

    /**
     * @return {string}
     */
    getStatusAsString(): string {
        if (this.isOffline()) {
            return 'offline';
        }

        return this.status ? this.status.toString() : 'N/A';
    }

    /**
     * @return {boolean}
     */
    isDead(): boolean {
        return this.status == CharacterStatus.dead;
    }

    /**
     * @return {boolean}
     */
    isOffline(): boolean {
        if (!this.last_activity_date || this.status == CharacterStatus.offline) {
            return true;
        }

        return Math.floor((Date.now() - this.last_activity_date.getTime()) / 1000) >= 600;
    }

    /**
     * @return {CharacterStatus}
     */
    getStatus(): CharacterStatus {
        return this.status;
    }

    /**
     * @return {Map<CharacterProp, any>}
     */
    getPayload(): Map<CharacterProp, any> {
        if (
            this.payload &&
            typeof this.payload == 'object' &&
            !('keys' in this.payload)
        ) {
            this.payload = objectToMap<CharacterProp, any>(this.payload || {});
        }

        return this.payload || new Map();
    }

    /**
     * @param {Map<CharacterProp, any> | Object} payload
     */
    setPayload(payload: Map<CharacterProp, any> | Object) {
        if (!('keys' in payload)) {
            payload = objectToMap<CharacterProp, any>(payload || {});
        }

        this.payload = <Map<CharacterProp, any>>payload;
    }

    public toJSON(): object {
        return {
            id: this._id.toString(),
            status: this.status.toString(),
            name: this.name,
            last_activity_date: this.last_activity_date
                ? this.last_activity_date.toISOString()
                : null,
            last_screen_url: process.env.SCREENSHOT_URL
                ? `${process.env.SCREENSHOT_URL}/${this.getName()}.jpg`
                : null,
            payload: mapToObject(this.getPayload()),
        }
    }
}
