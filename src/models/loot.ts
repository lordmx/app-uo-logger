import {
    Entity,
    ObjectID,
    ObjectIdColumn,
    Column,
    ManyToOne,
    Index,
    BeforeInsert,
} from "typeorm";
import { Character } from './character';
import { BodyParam } from "routing-controllers";
import * as crypto from 'crypto';

export interface LootOpt {
    character: Character | ObjectID;
    name: string;
    properties: Array<string>;
    meta: object;
}

@Entity({
    name: 'loot'
})
export class Loot {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    @Index()
    @ManyToOne(type => Character)
    @BodyParam('character', { required: true })
    character: Character | string;

    @Column()
    created_at: Date;

    @Column()
    @Index('properties-idx')
    name: string;

    @Column()
    @Index('properties-idx')
    properties_hash: string;

    @Column()
    properties: string[];

    @Column()
    meta: object;

    /**
     * @param {LootOpt} data
     */
    constructor(data: LootOpt) {
        Object.assign(this, data);
    }

    /**
     * @return {string}
     */
    getName(): string {
        return this.name;
    }

    /**
     * @return {string[]}
     */
    getProperties(): string[] {
        return this.properties || [];
    }

    @BeforeInsert()
    calculateHash() {
        this.properties_hash = Loot.getPropertiesHash(this.getProperties());
    }

    /**
     * @return {object}
     */
    public toJSON(): object {
        return {
            id: this._id.toString(),
            character: this.character instanceof Character
                ? this.character.toJSON()
                : null,
            created_at: this.created_at.toISOString(),
            name: this.name || null,
            properties: (this.properties || []).length > 0 ? this.properties : [],
        }
    }

    /**
     * @param {string[]} properties
     * @return {string}
     */
    static getPropertiesHash(properties: string[]): string {
        return crypto
            .createHash('md5')
            .update((properties || []).join('|'))
            .digest("hex");
    }
}
