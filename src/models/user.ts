import {
    Entity,
    ObjectID,
    ObjectIdColumn,
    Column,
    Index,
} from "typeorm";


@Entity({
    name: 'users'
})
export class User {

    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    name: string;

    @Column()
    @Index()
    token: string;
}
