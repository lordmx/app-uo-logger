export const CHARACTER_STATUS_CHANGED = 'character.status_changed';
export const CHARACTER_LOOT_RECEIVED = 'character.loot_received';
export const CHARACTER_NOTIFIED = 'character.notified';