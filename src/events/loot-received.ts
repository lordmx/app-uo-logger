import { Character } from "../models/character";
import { Loot } from "../models/loot";

export type LootReceivedEvent = {
    character: Character,
    loot: Loot,
}