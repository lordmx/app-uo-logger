import { Character } from "../models/character";
import { Notification } from "../notifications/interface";

export type NotificationEvent = {
    character: Character;
    notification: Notification;
}