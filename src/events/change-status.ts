import { Character, CharacterStatus } from "../models/character";

export type StatusChangedEvent = {
    character: Character;
    oldStatus: CharacterStatus;
    newStatus: CharacterStatus;
}