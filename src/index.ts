import 'dotenv/config';
import 'source-map-support/register';
import "reflect-metadata"

import { createKoaServer, useContainer } from "routing-controllers";
import { createSocketServer, SocketControllersOptions } from "socket-controllers";
import { Container } from "typedi";
import { createConnection, Connection } from "typeorm";
import { Server } from "net";
import { Config } from "./config";
import { EventEmitter } from 'events';
import { commandManager } from "./commands/manager";
import { NotificationManager } from "./notifications/manager";
import { CheckerManager } from "./checkers/manager";

// interceptors
import { JsonStringifyInterceptor } from './interceptors/json';

// middlewares
import { ErrorHandler } from "./middlewares/error";
import { RequestLogger } from "./middlewares/logger";

// controllers
import { CharactersController } from "./controllers/characters";
import { LootController } from "./controllers/loot";

// socket controllers
import { RpcController } from './socket-controllers/rpc';

// entities
import { Character } from "./models/character";
import { User } from "./models/user";
import { Loot } from "./models/loot";

// services
import { UserService } from './services/user';

// listeners
import { CharacterListener } from "./listeners/character";

// constants
import {
    CHARACTER_STATUS_CHANGED,
    CHARACTER_LOOT_RECEIVED,
    CHARACTER_NOTIFIED,
} from "./constants/events";

// commands
import { PingCommand } from "./commands/ping";
import { ListCommand } from "./commands/list";
import { StatusCommand } from "./commands/status";
import { LootCommand } from "./commands/loot";
import { PictureCommand } from "./commands/picture";

// notifications
import { LowDurability } from "./notifications/low-durability";
import { WeightLimit } from "./notifications/weight-limit";
import { LastShield } from "./notifications/last-shield";
import { LastWeapon } from "./notifications/last-weapon";

// checkers
import { OfflineChecker } from "./checkers/offline";

useContainer(Container);
const config: Config = Container.get(Config);

createConnection({
    type: 'mongodb',
    url: config.mongo.dsn,
    entities: [
        User,
        Character,
        Loot,
    ],
})
    .then((connection: Connection) => {
        console.log('mongo connected', connection.name);

        commandManager.register([
            new PingCommand(),
            new ListCommand(),
            new StatusCommand(),
            new LootCommand(),
            new PictureCommand(),
        ]);

        const checkerManager = Container.get(CheckerManager);
        checkerManager.register([
            new OfflineChecker(),
        ]);

    })
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

const characterListener: CharacterListener = Container.get(CharacterListener);
const emitter: EventEmitter = new EventEmitter();

emitter.on(
    CHARACTER_STATUS_CHANGED,
    characterListener.onStatusChanged.bind(characterListener)
);
emitter.on(
    CHARACTER_LOOT_RECEIVED,
    characterListener.onLootReceived.bind(characterListener)
);
emitter.on(
    CHARACTER_NOTIFIED,
    characterListener.onNotificationReceived.bind(characterListener)
);

Container.set(EventEmitter, emitter);

const notificationManager: NotificationManager = Container.get(NotificationManager);

notificationManager.register([
    new LowDurability(),
    new WeightLimit(),
    new LastShield(),
    new LastWeapon(),
]);

const koaApp = createKoaServer({
    routePrefix: '/api/v1',
    cors: true,
    controllers: [
        CharactersController,
        LootController,
    ],
    interceptors: [
        JsonStringifyInterceptor,
    ],
    middlewares: [
        ErrorHandler,
        RequestLogger,
    ],
    defaultErrorHandler: false,
    currentUserChecker: UserService.currentUserChecker,
    authorizationChecker: UserService.authorizationChecked,
});

const socketApp = createSocketServer(config.socketPort, <SocketControllersOptions>{
    controllers: [
        RpcController,
    ]
});

const server: Server = koaApp.listen(config.port, config.host);

server.on('error', (error: string) => {
    console.log('connection error', error);
});
