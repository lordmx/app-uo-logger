FROM node:carbon

WORKDIR /app

COPY . .

RUN set -e && \
    # Ignore package-lock.json for now
    rm -f package-lock.json && \
    npm install && \
    npm run build

EXPOSE 10778
CMD [ "node", "dist" ]